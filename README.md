# Keystone: An Open-Source Secure Enclave Framework for RISC-V Processors

![Documentation Status](https://readthedocs.org/projects/keystone-enclave/badge/)
[![Build Status](https://travis-ci.org/keystone-enclave/keystone.svg?branch=master)](https://travis-ci.org/keystone-enclave/keystone/)

Visit [Project Website](https://keystone-enclave.org) for more information.



# Documentation

See [docs](http://docs.keystone-enclave.org) for getting started.

# Work in progeress

 Integrating Keystone with our RISC-V processor!
 This repo has: keystone, rtl for TRNG & SHA3, and other drivers for simulation.
